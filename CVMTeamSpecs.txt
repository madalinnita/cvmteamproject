CVM Team

Gestionare resurse de invatare online (integrare Facebook share pe grupuri/pagini)

->Specificatii:

   Aplicatie Web care sa contina resurse de invatare online si care sa integreze Facebook share pe grupuri/pagini.
             Structura site-ului este urmatoarea: 
•	 Headerul paginii este format dintr-un navigation bar cu urmatoarele optiuni: 
   -Home
   -Resurse 
   -Upload
   -Forum
   -Contact 
•	Body: se actualizeaza in functie de optiunea selectata de utilizator.
-Home: pagina principala in care sunt afisate date informative despre aplicatia web,
scopul si continutul ei. In pagina de Home se vor gasi urmatoarele componente:interfata
de logare a utilizatorilor via Facebook, dupa logare se vor afisa date despre utilizator,
precum: nume,fotografia de profil si alte detalii despre utilizator.
-Resurse: toate materiile disponibile si resursele aferente de invatare cu posibilitatea de 
share a acestora via Facebook in cadrul grupurilor/paginilor.
-Upload: pagina dedicata crearii de catre utilizatori de noi resurse si materiale care pot fi
accesate ulterior de ceilalti utilizatori din tabul Resurse.
-Forum: sectiune a aplicatiei web unde utilizatorii pot adresa intrebari si pot rezolva  problemele altor utilizatori.
-Contact: utilizatorii pot contacta administratorii site-ului pentru sesizarea de diverse buguri sau trimiterea unor sugestii. 

•	Footer: Social Media ale paginii.


    Descrierea proiectului:
    
    Proiectul nostru consta intr-o aplicatie web ce satisfice nevoia utilizatorilor
    de a face rost de anumite resurse de invatare cu posibilitatea de share pe Facebook.
    Utilizatorii pot gasi solutii la eventuale probleme prin intermediul forumului,
    unde pot adresa intrebari altor utilizatori si pot raspunde la randul lor.
    Daca acestia sesizeaza vreo problema sau vor sa ofere sugestii, pot contacta 
    administratorii paginii prin optiunea de Contact.
     De asemenea, acestia pot incarca resurse pentru diferite materii care pot fi accesate de oricine.



